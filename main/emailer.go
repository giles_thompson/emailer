/** 
Sends standardised Emails via the SMTP protocol,the application is easily
configurable, developers need only edit the defined placeholder values to 
point to their specific SMTP server.
@Auther Giles Thompson.
*/
package main

import (
	"log"
	"net/smtp"
    "fmt"
)



func main(){

// Set up authentication information.
auth := smtp.PlainAuth(
	"",
	"[username]",
	"[passphrase]",
	"[SMTP Server Address]",

)
// Connect to the server, authenticate, s	"fmt"et the sender and recipient,
// and send the email all in one step.
err := smtp.SendMail(
	"[SMTP Server Address]:[SMTP Server Port]",
    auth,
	"[Sender Address]",
	[]string{"[Recipient Address]"},
	[]byte("[Message]"),

)
if err != nil {
	fmt.Print("Sending message failed.");
	log.Fatal(err)
}
fmt.Print("Message  successfully sent!!");
}